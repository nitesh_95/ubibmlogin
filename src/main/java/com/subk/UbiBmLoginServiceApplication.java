package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient

public class UbiBmLoginServiceApplication {

	public static void main(String[] args) {
		System.out.println("");
		SpringApplication.run(UbiBmLoginServiceApplication.class, args);
	}

}
