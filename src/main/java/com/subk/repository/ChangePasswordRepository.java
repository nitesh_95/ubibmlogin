package com.subk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.ManagerPasswordHistory;

@Repository
public interface ChangePasswordRepository extends JpaRepository<ManagerPasswordHistory, String>{
	
	 @Query(value="select * from manager_password_history where solid='8000' order by id desc,password_updated_datetime desc limit 2",nativeQuery=true)
	 List<ManagerPasswordHistory> getoldpasswordList(String solid);

}
