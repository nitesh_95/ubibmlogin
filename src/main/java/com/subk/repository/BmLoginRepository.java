package com.subk.repository;


import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.subk.entity.ManagerLogin;
@Repository
public interface BmLoginRepository extends JpaRepository<ManagerLogin, String>{
	
   @Query("from ManagerLogin where solid=?1")
   ManagerLogin login(String userid);
   
   @Transactional
   @Modifying
   @Query("update ManagerLogin  set password =:newpass,passwordUpdateddate=:passwordUpdateddate WHERE solid =:solid")
   int updatePassword(String newpass,String solid,Date passwordUpdateddate);

   @Transactional
   @Modifying
   @Query("update ManagerLogin  set status =:status,failLoginAttempts=:failLoginAttempts WHERE solid =:solid")
   int updateLoginStatus(String status,int failLoginAttempts,String solid);
	 
}
