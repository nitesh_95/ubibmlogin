package com.subk.services;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.subk.entity.ManagerLogin;
import com.subk.entity.ManagerPasswordHistory;
import com.subk.enums.LoginStatus;
import com.subk.repository.BmLoginRepository;
import com.subk.repository.ChangePasswordRepository;
import com.subk.util.LoginReponse;
import com.subk.util.PasswordEncoder;

@Service
public class BmLoginServiceImpl implements BmLoginService {
	Logger log = LoggerFactory.getLogger(BmLoginServiceImpl.class);
	
	@Autowired
	private BmLoginRepository bmLoginRepository;
	
	@Autowired
	private ChangePasswordRepository changepassRep;
	
	@Value("${passminlength}")
	private int passminlength;
	
	@Value("${passMaxlength}")
	private int passMaxlength;
	
	@Value("${password_expirein}")
	private int password_expirein;
	
	@Value("${login_attempts}")
	private int login_attempts;
	
	@Value("${password_expire_notification_in}")
	private int pswd_expr_notif_in;
	
	private String expireInmsg;

	ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
	
	@Override
	public LoginReponse managerLogin(String solid, String password) {
		log.info("***********managerLogin() Start*******************************");
		LoginReponse reponse = new LoginReponse();
		try {
			ManagerLogin managerLogin = bmLoginRepository.login(solid);
			log.info("******Manager login details: "+managerLogin.toString());
			
			if (managerLogin != null&&managerLogin.getStatus().equalsIgnoreCase("Active")) {
				String encryptpassword = encoder.encodePassword(password, managerLogin.getPasswordSalt());
				if (encryptpassword.equals(managerLogin.getPassword())) {
					if(managerLogin.getPasswordUpdateddate()!=null&&isPasswordExpired(managerLogin.getPasswordUpdateddate())) {
						reponse.setIsrequiredChangePassword(true);
					}
					bmLoginRepository.updateLoginStatus(LoginStatus.Active.name(),0, solid);
					reponse.setSolid(managerLogin.getSolid());
					reponse.setPasswordExipreNotifi(expireInmsg);
					reponse.setBranchname(managerLogin.getBranchName());
					reponse.setPortaltype(managerLogin.getPortaltype());
					reponse.setStatus(HttpStatus.SC_OK);
					reponse.setReason("Success");
				} else {
					int loginattempt = managerLogin.getFailLoginAttempts()+1; 
					int noOfAttemptsRemain = login_attempts-loginattempt;
					reponse.setStatus(HttpStatus.SC_UNAUTHORIZED);
					if(loginattempt>=login_attempts) {
						bmLoginRepository.updateLoginStatus(LoginStatus.InActive.name(),loginattempt, solid);
						reponse.setReason("Your have "+login_attempts+" consecutive wrong entries. Your access is disabled, For support please contact Sub-K Team.");
					}else {
						bmLoginRepository.updateLoginStatus(LoginStatus.Active.name(),loginattempt, solid);
						reponse.setReason("Invalid credentials, your access will be disabled after "+login_attempts+" consecutive wrong entries. you have Remain "+noOfAttemptsRemain+" more entries");
					}
				}
			} else {
				reponse.setStatus(HttpStatus.SC_NO_CONTENT);
				reponse.setReason("Invalid credentials OR Invalid employee status, For support please contact Sub-K Team.");
			}
		} catch (Exception e) {
			log.info("********************Exception while login: "+e.getMessage());
			e.getStackTrace();
			reponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
			reponse.setReason("Unable to process the request, Please try again");
		}
		return reponse;
	}
	
	public LoginReponse updateNewPassword(String solid, String newpassword, String oldpassword) {
		log.info("********************updateNewPassword service START**************************");
		log.info("solid:::::::::: " + solid + "newpassword:::::::::::::::: " + newpassword + "oldpassword:::::::: "
				+ oldpassword);
		LoginReponse response = new LoginReponse();
		int count = 0;
		try {
			ManagerLogin mangerlogin = bmLoginRepository.login(solid);
			log.info("mangerlogin:::::::::::::::::::::::  " + mangerlogin);
			if (mangerlogin==null) {
				response.setReason("Invalid solid");
				response.setStatus(HttpStatus.SC_BAD_REQUEST);
				return response;
			}
			if (newpassword.length() < passminlength || newpassword.length() > passMaxlength) {
					response.setReason("password criteria not matched");
					response.setStatus(HttpStatus.SC_BAD_REQUEST);
					return response;
				}
					String oldpasswordsalt = mangerlogin.getPasswordSalt();
					String oldencrptedpin = mangerlogin.getPassword();
					log.info("oldencrptedpin::::::::::::::::: " + oldencrptedpin);
					String newencrptedPin = encoder.encodePassword(newpassword, oldpasswordsalt);
					log.info("newencrptedPin::::::::::::::: " + newencrptedPin);
					if (!newencrptedPin.equals(oldencrptedpin)) {
						List<ManagerPasswordHistory> passhislist = changepassRep.getoldpasswordList(solid);
						if (CollectionUtils.isEmpty(passhislist)) {
							saveOrUpdatePasswordHistory(solid, oldencrptedpin);
						} else {
							for (ManagerPasswordHistory passhist : passhislist) {
								if (passhist.getOldPassword().equals(newencrptedPin)) {
									response.setStatus(HttpStatus.SC_FORBIDDEN);
									response.setReason("Password should not match with the last 3 consecutive passwords.");
									return response;
								}
							}
							saveOrUpdatePasswordHistory(solid, oldencrptedpin);
						}
						count = bmLoginRepository.updatePassword(newencrptedPin, solid,new Timestamp(System.currentTimeMillis()));
						if (count > 0) {
							response.setReason("success");
							response.setStatus(HttpStatus.SC_OK);
						} else {
							response.setReason("Failed to update the password,Please try again");
							response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
						}
					} else {
						response.setReason("Password should not match with the last 3 consecutive passwords.");
						response.setStatus(HttpStatus.SC_UNAUTHORIZED);
					}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception while updating new password :  " +e.getMessage());
			response.setReason("Unable to change the password, Please try again.");
			response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		}
		log.info("response:::::::::::::::::::::::::::::::::  " + response);
		log.info("********************updateNewPassword service End**************************");
		return response;

	}
	 public String getSalt(int length){
	        StringBuffer buffer=new StringBuffer();
	        int minimum=33,maximum=120,randomNum;
	        for(int i=0;i<length;i++){
	            randomNum =   (int)(Math.random() * maximum); 
	            if(randomNum < minimum){
	                randomNum += minimum;
	            }
	            buffer.append((char)randomNum);
	            
	        }
	        return buffer.toString();
	    }
		
		private ManagerPasswordHistory saveOrUpdatePasswordHistory(String solId,String oldpassword) throws Exception {
			log.info("********************saveOrUpdatePasswordHistory service Start**************************");
			ManagerPasswordHistory history = new ManagerPasswordHistory();
			history.setOldPassword(oldpassword);
			history.setPasswordUpdatedDatetime(new Timestamp(System.currentTimeMillis()));
			history.setSolid(solId);
			history = changepassRep.save(history);
			log.info("********************saveOrUpdatePasswordHistory service End**************************");
			return history;
			
		}
		
	public boolean isPasswordExpired(Date passwordUpdateddate) {
		log.info("****************isPasswordExpired() checking Start*****************");
		boolean isPasswordExpired = false;
		try {
			Calendar passwordExpiredIn = Calendar.getInstance();
			passwordExpiredIn.setTime(passwordUpdateddate);
			passwordExpiredIn.add(Calendar.DATE, 30);
			
			Date passwordExpireDate = passwordExpiredIn.getTime();
			log.info("passwordExpiredIn.getTime() " + passwordExpireDate);
			
			Date current_date = new Date();
			log.info("current_date " + current_date);

			if (current_date.compareTo(passwordExpireDate) > 0) {
				log.info("Password Expired  ture:: " + current_date.compareTo(passwordExpiredIn.getTime()));
				isPasswordExpired = true;
			} else {
				log.info("Password Expired  false:: " + current_date.compareTo(passwordExpiredIn.getTime()));
				isPasswordExpired = false;
			}
			long difference = current_date.getTime()-passwordExpireDate.getTime();
			long exprInDays = (difference / (1000*60*60*24));
			System.out.println("aaaaaaaaa "+exprInDays);
			if(exprInDays>0&&exprInDays<=pswd_expr_notif_in) {
				expireInmsg="Your password will be expire in "+exprInDays+" days, we wish you to reset before expired.";
			}else {
				expireInmsg="";
			}
		} catch (Exception e) {
			e.printStackTrace();
			isPasswordExpired = true;
			log.info("****************Exception while checking isPasswordExpired(): " + e.getMessage());
		}
		log.info("****************isPasswordExpired() checking Start*****************");
		return isPasswordExpired;
	}
	
	public void updatedefaultPassword() {
		System.out.println("*******update password start***********");
		List<ManagerLogin> logindetails = bmLoginRepository.findAll();
		if(logindetails.size()>0) {
			ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
			for(ManagerLogin login : logindetails) {
				String passwdSalt = PasswordEncoder.getSalt(10);
				String encodedPswd = encoder.encodePassword("Abc@1234",passwdSalt);
				login.setPasswordSalt(passwdSalt);
				login.setPassword(encodedPswd);
				bmLoginRepository.save(login);
				System.out.println(" aaaaaaa   "+login.toString());
			}
		}
		System.out.println("*******update password end***********");
	}
}
