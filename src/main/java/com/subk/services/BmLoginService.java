package com.subk.services;

import com.subk.util.LoginReponse;

public interface BmLoginService {
 
	public LoginReponse managerLogin(String solid, String password);
	public LoginReponse updateNewPassword(String solid, String newpassword, String oldpassword);
	public void updatedefaultPassword();
}
