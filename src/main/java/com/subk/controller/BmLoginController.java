package com.subk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.subk.services.BmLoginServiceImpl;
import com.subk.util.LoginReponse;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
@CrossOrigin
@RestController
public class BmLoginController {
	
	Logger log = LoggerFactory.getLogger(BmLoginController.class);
	@Autowired
	BmLoginServiceImpl bmLoginServiceImpl;
	
	@RequestMapping("/login")
	public LoginReponse login(@RequestParam(name="UserId") String userid,@RequestParam(name="Password") String password) {
		log.info("************login() Start*************************");
		log.info("userid::::::::"+userid+"  password::::::"+password);
		LoginReponse loginresponse = bmLoginServiceImpl.managerLogin(userid,password);
		log.info("*********Login Response:::::::: "+loginresponse.toString());
		log.info("************login() End*************************");
		return loginresponse;
	}
	
	@RequestMapping("/changepassword")
	public LoginReponse updatePassword(@RequestParam(name = "Solid") String solid,
			@RequestParam(name = "oldPassword") String oldpassword,
			@RequestParam(name = "newPassword") String newpassword) {
		log.info("******************************updatePassword controller START*************************");
		log.info("solid:::::::::::::" + solid + "oldpassword::::::::::::::  " + oldpassword
				+ "newpassword:::::::::::::::::::::: " + newpassword);
		LoginReponse response = bmLoginServiceImpl.updateNewPassword(solid, newpassword, oldpassword);
		log.info("response::::::::::::::::::  " + response);
		log.info("******************************updatePassword controller End*************************");
		return response;
	}

	@GetMapping("/updatedefaultpassword")
	public void updatedefaultPassword() {
		log.info("******************************updatePassword controller START*************************");
		bmLoginServiceImpl.updatedefaultPassword();
		log.info("******************************updatePassword controller End*************************");
	}

}
