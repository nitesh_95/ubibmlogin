package com.subk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="manager_password_history")
public class ManagerPasswordHistory {

		@Id
		@Column(name = "id")
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private String id;
		@Column(name = "solid")
		private String solid;
		@Column(name = "old_password")
		private String oldPassword;
		@Column(name = "password_updated_datetime")
		private Date passwordUpdatedDatetime;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getSolid() {
			return solid;
		}
		public void setSolid(String solid) {
			this.solid = solid;
		}
		public String getOldPassword() {
			return oldPassword;
		}
		public void setOldPassword(String oldPassword) {
			this.oldPassword = oldPassword;
		}
		public Date getPasswordUpdatedDatetime() {
			return passwordUpdatedDatetime;
		}
		public void setPasswordUpdatedDatetime(Date passwordUpdatedDatetime) {
			this.passwordUpdatedDatetime = passwordUpdatedDatetime;
		}
		@Override
		public String toString() {
			return "Manager_password_history [id=" + id + ", solid=" + solid + ", oldPassword=" + oldPassword
					+ ", passwordUpdatedDatetime=" + passwordUpdatedDatetime + "]";
		}
	
		
		
	
}
