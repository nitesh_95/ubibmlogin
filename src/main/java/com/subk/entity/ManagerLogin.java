package com.subk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ManagerLogin")
public class ManagerLogin {
	
	@Id
	@Column(name = "solid")
	private String solid;
	@Column(name = "branchname")
	private String branchName;
	@Column(name = "createddatetime")
	private String createddatetime;
	@Column(name = "status")
	private String status;
	@Column(name = "password")
	private String password;
	@Column(name = "roid")
	private int roid;
	@Column(name="password_salt")
	private String passwordSalt;
	@Column(name = "portaltype")
	private String portaltype;
	private Date passwordUpdateddate;
	private int failLoginAttempts;
	
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCreateddatetime() {
		return createddatetime;
	}
	public void setCreateddatetime(String createddatetime) {
		this.createddatetime = createddatetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPortaltype() {
		return portaltype;
	}
	public void setPortaltype(String portaltype) {
		this.portaltype = portaltype;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getRoid() {
		return roid;
	}
	public void setRoid(int roid) {
		this.roid = roid;
	}
	public String getPasswordSalt() {
		return passwordSalt;
	}
	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}
	public Date getPasswordUpdateddate() {
		return passwordUpdateddate;
	}
	public void setPasswordUpdateddate(Date passwordUpdateddate) {
		this.passwordUpdateddate = passwordUpdateddate;
	}
	public int getFailLoginAttempts() {
		return failLoginAttempts;
	}
	public void setFailLoginAttempts(int failLoginAttempts) {
		this.failLoginAttempts = failLoginAttempts;
	}
	@Override
	public String toString() {
		return "ManagerLogin [solid=" + solid + ", branchName=" + branchName + ", createddatetime=" + createddatetime
				+ ", status=" + status + ", password=" + password + ", roid=" + roid + ", passwordSalt=" + passwordSalt
				+ ", portaltype=" + portaltype + ", passwordUpdateddate=" + passwordUpdateddate + ", failLoginAttempts="
				+ failLoginAttempts + "]";
	}
	

}
