package com.subk.util;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import ch.qos.logback.core.encoder.Encoder;

public class PasswordEncoder {

	
	
	public static String getSalt(int length){
        StringBuffer buffer=new StringBuffer();
        int minimum=33,maximum=120,randomNum;
        for(int i=0;i<length;i++){
            randomNum =   (int)(Math.random() * maximum); 
            if(randomNum < minimum){
                randomNum += minimum;
            }
            buffer.append((char)randomNum);
            
        }
        return buffer.toString();
    }
	
	public static void main(String args[]) {
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
		String passwdSalt = getSalt(10);
		String encodedPswd = encoder.encodePassword("Abc@1234","BLneecuj]@");
		System.out.println("Password: "+encodedPswd+ "  salt:"+passwdSalt);
		//BLneecuj]@
		//8b43c44b44d637c9e673add1207c8f9770e08e0c541fe560a4e6790a415a2328
		//8b43c44b44d637c9e673add1207c8f9770e08e0c541fe560a4e6790a415a2328
		
	}
}
