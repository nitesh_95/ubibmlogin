package com.subk.util;

public class LoginReponse {
	private int status;
	private String reason;
	private String solid;
	private String branchname;
	private String portaltype;
	private boolean isrequiredChangePassword;
	private String passwordExipreNotifi;
	
	public String getPortaltype() {
		return portaltype;
	}
	public void setPortaltype(String portaltype) {
		this.portaltype = portaltype;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	
	public boolean isIsrequiredChangePassword() {
		return isrequiredChangePassword;
	}
	public void setIsrequiredChangePassword(boolean isrequiredChangePassword) {
		this.isrequiredChangePassword = isrequiredChangePassword;
	}
	public String getPasswordExipreNotifi() {
		return passwordExipreNotifi;
	}
	public void setPasswordExipreNotifi(String passwordExipreNotifi) {
		this.passwordExipreNotifi = passwordExipreNotifi;
	}
	@Override
	public String toString() {
		return "LoginReponse [status=" + status + ", reason=" + reason + ", solid=" + solid + ", branchname="
				+ branchname + ", portaltype=" + portaltype + ", isrequiredChangePassword=" + isrequiredChangePassword
				+ ", passwordExipreNotifi=" + passwordExipreNotifi + "]";
	}
	
	

}
